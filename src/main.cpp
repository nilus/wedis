
#include "WedisConfig.h"

#include "core/web/Server.hpp"

#include "wedis/Storage.hpp"
#include "wedis/endpoint/Admin.hpp"
#include "wedis/endpoint/Client.hpp"
#include "wedis/endpoint/Open.hpp"
#include "wedis/resource/admin/Company.hpp"
#include "wedis/resource/client/Company.hpp"
#include "wedis/resource/open/Log.hpp"

#define PORT "8001"

#ifdef _WIN32
#define ANY_ADDRESS U("*")
#else
#define ANY_ADDRESS "0.0.0.0"
#endif

namespace
{
	web::uri uri(const std::string & path, const std::string & address, const std::string & port)
	{
		web::http::uri_builder builder{};
		builder
			.set_scheme("http")
			.set_host(address)
			.set_port(port)
			.set_path(path);

		return builder.to_uri();
	}
}

int main()
{
	printf("Wedis - starting: version %d.%d.%d.%d\n\n",
		Wedis_VERSION_MAJOR,
		Wedis_VERSION_MINOR,
		Wedis_VERSION_PATCH,
		Wedis_VERSION_TWEAK);

	// TODO: logger

	// TODO: console input

	wedis::Storage storage{};

	// Server instance
	core::web::Server server{ uri("wedis", ANY_ADDRESS, PORT), "Wedis"};

	// Admin endpoint and resources
	wedis::endpoint::Admin admin{ storage, {"tomato", "potato"} };
	wedis::resource::admin::Company admin_company{ admin.path, storage };
	admin.handle_resource(admin_company);
	server.handle(admin);

	// Client endpoint and resources
	wedis::endpoint::Client client{ storage };
	wedis::resource::client::Company client_company{ client.path, storage };
	client.handle_resource(client_company);
	server.handle(client);

	// Open endpoint and resources
	wedis::endpoint::Open open{ };
	wedis::resource::open::Log open_log{ open.path };
	open.handle_resource(open_log);
	server.handle(open);

	server.start();
	{
		std::cout << std::endl << "Press ENTER to exit." << std::endl;
		std::string line;
		std::getline(std::cin, line);
	}
	server.stop();

	printf("Wedis - existing\n");
	return 0;
}
