
#ifndef WEDIS_RESOURCE_ADMIN_COMPANY_HPP
#define WEDIS_RESOURCE_ADMIN_COMPANY_HPP

#include "Base.hpp"

#include "wedis/Storage.hpp"

namespace wedis
{
	namespace resource
	{
		namespace admin
		{
			class Company : public BaseResource
			{
			private:

				IStorage & storage;

			public:

				explicit Company(const std::string & absolute_path, IStorage & storage)
					: BaseResource(absolute_path + "/company", "/company")
					, storage(storage)
				{}

			public:

				void handle_delete(
					const http_request & request,
					const path_params_t & params) const override
				{
					if (params.empty())
					{
						throw core::exception::bad_data_t{};
					}

					this->storage.remove(params[0]);

					request.reply(status_codes::OK);
				}

				void handle_get(
					const http_request & request,
					const path_params_t & params) const override
				{
					auto companies = this->storage.all();

					json jc = json::array(companies.size());

					unsigned index = 0;
					for (auto & company : companies)
					{
						jc.as_array()[index++] = company.to_json();
					}

					request.reply(status_codes::OK, jc);
				}

				void handle_post(
					const http_request & request,
					const path_params_t & params,
					const json & jdata) const override
				{
					this->storage.create(model::company_t{ jdata });

					request.reply(status_codes::OK);
				}
			};
		}
	}
}

#endif //WEDIS_RESOURCE_ADMIN_COMPANY_HPP
