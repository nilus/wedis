
#ifndef WEDIS_RESOURCE_OPEN_LOG_HPP
#define WEDIS_RESOURCE_OPEN_LOG_HPP

#include "Base.hpp"

#include "wedis/Storage.hpp"

namespace wedis
{
	namespace resource
	{
		namespace open
		{
			class Log : public BaseResource
			{
			public:

				explicit Log(const std::string & absolute_path)
					: BaseResource(absolute_path + "/company", "/company")
				{}

			public:

				void handle_get(
					const http_request & request,
					const path_params_t & params) const override;

				void handle_post(
					const http_request & request,
					const path_params_t & params,
					const json & jdata) const override;
			};
		}
	}
}

#endif // !WEDIS_RESOURCE_OPEN_LOG_HPP
