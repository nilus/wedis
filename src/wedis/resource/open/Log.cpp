
#include "Log.hpp"

#include <chrono>
#include <fstream>
#include <mutex>

#include <cpprest/filestream.h>

namespace wedis
{
	namespace resource
	{
		namespace open
		{
			constexpr auto FILENAME = U("output.txt");

			void log_json(const ::web::json::value & jdata)
			{
				static std::mutex mutex;

				auto time_now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());

				std::cout << std::ctime(&time_now) << "Log request called" << std::endl;

				std::lock_guard<std::mutex> lock{ mutex };

				std::ofstream output{ FILENAME, std::ofstream::out | std::ofstream::app };

				if (output.is_open())
				{
					output << std::ctime(&time_now) << jdata.serialize() << std::endl;
					output.close();
				}
				else
				{
					std::cerr << "Could not open file!" << std::endl;
				}
			}

			void Log::handle_get(const http_request & request, const path_params_t & params) const
			{
				// async file stream example
				// https://msdn.microsoft.com/en-us/library/jj950081.aspx

				auto task = concurrency::streams::file_stream<unsigned char>::open_istream(FILENAME);

				auto filestream = task.get();

				request.reply(::web::http::status_codes::OK, filestream).then([filestream]()
                {
	                filestream.close();
                });
			}

			void Log::handle_post(
				const http_request & request,
				const path_params_t & params,
				const json & jdata) const
			{
				log_json(jdata);

				// reply OK
				request.reply(status_codes::OK);
			}
		}
	}
}
