
#ifndef WEDIS_RESOURCE_CLIENT_COMPANY_HPP
#define WEDIS_RESOURCE_CLIENT_COMPANY_HPP

#include "Base.hpp"

#include "wedis/Storage.hpp"

namespace wedis
{
	namespace resource
	{
		namespace client
		{
			class Company : public BaseResource
			{
			private:

				IStorage & storage;

			public:

				explicit Company(const std::string & absolute_path, IStorage & storage)
					: BaseResource(absolute_path + "/company", "/company")
				    , storage(storage)
				{}

			public:

				void handle_get(
					const http_request & request,
					const path_params_t & params,
					const model::company_t & company) const override
				{
					request.reply(status_codes::OK, company.to_json());
				}

				void handle_patch(
					const http_request & request,
					const path_params_t & params,
					const model::company_t & company,
					const json & jdata) const override
				{
					auto updated = company;
					updated.update(jdata);

					this->storage.update(std::move(updated));

					request.reply(status_codes::OK);
				}
			};
		}
	}
}

#endif // ! WEDIS_RESOURCE_CLIENT_COMPANY_HPP
