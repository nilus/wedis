
#ifndef WEDIS_RESOURCE_CLIENT_BASE_HPP
#define WEDIS_RESOURCE_CLIENT_BASE_HPP

#include "wedis/model/company.hpp"
#include "core/web/Resource.hpp"

namespace wedis
{
	namespace resource
	{
		namespace client
		{
			class BaseResource : public core::web::Resource
			{
			public:

				BaseResource(std::string && absolute_path, std::string && relative_path)
					: core::web::Resource(std::move(absolute_path), std::move(relative_path))
				{}

			public:

				virtual void handle_get(
					const http_request & request,
					const path_params_t & params,
					const model::company_t & company) const
				{
					throw core::exception::method_unsupported_t{};
				}

				virtual void handle_patch(
					const http_request & request,
					const path_params_t & params,
					const model::company_t & company,
					const json & jdata) const
				{
					throw core::exception::method_unsupported_t{};
				}
			};
		}
	}
}

#endif //WEDIS_RESOURCE_CLIENT_BASE_HPP
