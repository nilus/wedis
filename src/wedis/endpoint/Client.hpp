
#ifndef WEDIS_ENDPOINT_CLIENT_HPP
#define WEDIS_ENDPOINT_CLIENT_HPP

#include "core/web/Endpoint.hpp"

#include "wedis/Storage.hpp"
#include "wedis/resource/client/Base.hpp"

namespace wedis
{
	namespace endpoint
	{
		class Client : public core::web::Endpoint<resource::client::BaseResource>
		{
		private:

			IStorage & storage;

		public:

			explicit Client(IStorage & storage)
				: core::web::Endpoint<resource::client::BaseResource>("/client")
				, storage(storage)
			{
			}

		protected:

			void process(const http_request & request) override
			{
				auto credentials = core::web::credentials(request.headers());

				auto company = this->storage.authenticate(credentials);

				auto relative_path = request.relative_uri().path();

				auto resource = match(relative_path);

				// remove first item which is "client" since it it not part of the "resource" path
				auto params = ::web::uri::split_path(relative_path);
				params.erase(params.begin());

				auto & method = request.method();

				// NOTE: add methods here if needed by resources

				if (method == ::web::http::methods::GET)
				{
					resource->handle_get(request, params, company);
				}
				else if (method == ::web::http::methods::PATCH)
				{
					resource->handle_patch(request, params, company, extract_json(request));
				}
				else
				{
					throw core::exception::method_unsupported_t{};
				}
			}
		};
	}
}

#endif // ! WEDIS_ENDPOINT_CLIENT_HPP
