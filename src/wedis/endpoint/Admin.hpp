
#ifndef WEDIS_ENDPOINT_ADMIN_HPP
#define WEDIS_ENDPOINT_ADMIN_HPP

#include "core/web/Endpoint.hpp"

#include "wedis/Storage.hpp"
#include "wedis/resource/admin/Base.hpp"

namespace wedis
{
	namespace endpoint
	{
		class Admin : public core::web::Endpoint<resource::admin::BaseResource>
		{
		private:

			const core::web::credentials_t credentials;

			IStorage & storage;

		public:

			explicit Admin(IStorage & storage, core::web::credentials_t && credentials)
				: core::web::Endpoint<resource::admin::BaseResource>("/admin")
				, credentials(std::move(credentials))
				, storage(storage)
			{
			}

		protected:

			void process(const http_request & request) override
			{
				core::web::authenticate(request.headers(), this->credentials);

				auto relative_path = request.relative_uri().path();

				auto resource = match(relative_path);

				// remove first item which is "admin" since it it not part of the "resource" path
				auto params = ::web::uri::split_path(relative_path);
				params.erase(params.begin());

				auto & method = request.method();

				// NOTE: add methods here if needed by resources

				if (method == ::web::http::methods::GET)
				{
					resource->handle_get(request, params);
				}
				else if (method == ::web::http::methods::DEL)
				{
					resource->handle_delete(request, params);
				}
				else if (method == ::web::http::methods::POST)
				{
					resource->handle_post(request, params, extract_json(request));
				}
				else
				{
					throw core::exception::method_unsupported_t{};
				}
			}
		};
	}
}

#endif // ! WEDIS_ENDPOINT_ADMIN_HPP
