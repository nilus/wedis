
#ifndef WEDIS_ENDPOINT_LOG_HPP
#define WEDIS_ENDPOINT_LOG_HPP

#include "core/web/Endpoint.hpp"

#include "wedis/resource/open/Base.hpp"

namespace wedis
{
	namespace endpoint
	{
		class Open : public core::web::Endpoint<resource::open::BaseResource>
		{
		public:

			Open() : core::web::Endpoint<resource::open::BaseResource>("")
			{}

		protected:

			void process(const http_request & request) override
			{
				auto relative_path = request.relative_uri().path();

				auto resource = match(relative_path);

				auto & method = request.method();

				// NOTE: add methods here if needed by resources

				if (method == ::web::http::methods::GET)
				{
					resource->handle_get(request, ::web::uri::split_path(relative_path));
				}
				else if (method == ::web::http::methods::POST)
				{
					resource->handle_post(request, ::web::uri::split_path(relative_path), extract_json(request));
				}
				else
				{
					throw core::exception::method_unsupported_t{};
				}
			}
		};
	}
}

#endif // !WEDIS_ENDPOINT_LOG_HPP
