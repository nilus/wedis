
#include "Storage.hpp"

namespace wedis
{
	std::vector<model::company_t> Storage::all() const
	{
		std::vector<model::company_t> companies;
		companies.reserve(this->companies.size());

		for (auto & pair : this->companies)
		{
			companies.push_back(pair.second);
		}

		return companies;
	}

	model::company_t Storage::authenticate(const core::web::credentials_t & credentials) const
	{
		std::lock_guard<std::mutex> lock{this->mutex};

		auto itr = this->companies.find(credentials.first);

		if (itr == this->companies.end())
		{
			throw core::exception::not_found_t{};
		}

		auto & company = *itr;

		if (company.second.secret != credentials.second)
		{
			throw core::exception::unauthorized_t{};
		}

		return company.second;
	}

	void Storage::create(model::company_t && company)
	{
		std::lock_guard<std::mutex> lock{this->mutex};

		auto itr = this->companies.find(company.id);

		if (itr != this->companies.end())
		{
			throw core::exception::error_t{ "Company is already registered." };
		}

		auto r = this->companies.insert({ company.id, company });

		if (!r.second)
		{
			throw core::exception::error_t{};
		}
	}

	void Storage::update(model::company_t && updated)
	{
		std::lock_guard<std::mutex> lock{this->mutex};

		auto itr = this->companies.find(updated.id);

		if (itr == this->companies.end())	// whops!
		{
			throw core::exception::error_t{ };
		}

		(*itr).second = updated;
	}

	void Storage::remove(const std::string & unique)
	{
		std::lock_guard<std::mutex> lock{this->mutex};

		auto itr = this->companies.find(unique);

		if (itr == this->companies.end())
		{
			throw core::exception::not_found_t{};
		}

		this->companies.erase(itr);
	}
}
