
#ifndef WEDIS_STORAGE_HPP
#define WEDIS_STORAGE_HPP

#include "core/exception.hpp"
#include "core/web/utils.hpp"

#include "wedis/model/company.hpp"

#include <mutex>
#include <unordered_map>

namespace wedis
{
    class IStorage
    {
	public:

		virtual std::vector<model::company_t> all() const = 0;

		// lookup registered company with 'unique'
		// authenticate with 'secret'
		// returns company if successful
		// throws
		// - not_found; if 'unique' is not found
		// - unauthorized; if 'secret' does not match
		virtual model::company_t authenticate(const core::web::credentials_t & credentials) const = 0;

		virtual void create(model::company_t && company) = 0;

		virtual void update(model::company_t && company) = 0;

		virtual void remove(const std::string & unique) = 0;
    };


    class Storage : public IStorage
    {
	private:

		mutable std::mutex mutex;
		std::unordered_map<std::string, model::company_t> companies;

	public:

		Storage() = default;

	public:

		std::vector<model::company_t> all() const override;

		model::company_t authenticate(const core::web::credentials_t & credentials) const override;

		void create(model::company_t && company) override;

		void update(model::company_t && updated) override;

		void remove(const std::string & unique) override;

	private:

    };
}

#endif // !WEDIS_STORAGE_HPP
