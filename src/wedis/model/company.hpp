
#ifndef WEDIS_MODEL_COMPANY_HPP
#define WEDIS_MODEL_COMPANY_HPP

#include <string>

#include <core/json.hpp>

namespace wedis
{
	namespace model
	{
		struct company_t
		{
			// company unique id - login purpose
			std::string id;

			// company display name
			std::string name;

			// company secret code - login purpose
			std::string secret;

			// company server hostname
			std::string host;

			// company server port
			int port;

			company_t() = default;

			company_t(const company_t & other) = default;

			company_t(company_t && other) = default;

			explicit company_t(const json & data)
				: id(data.at(U("id")).as_string())
				, name(data.at(U("name")).as_string())
				, secret(data.at(U("secret")).as_string())
				, host(data.at(U("host")).as_string())
				, port(data.at(U("port")).as_integer())
			{
			}

			company_t & operator = (const company_t & other) = default;

			company_t & operator = (company_t && other) = default;

			void update(const json & data)
			{
				if (data.has_field(U("name")))
					this->name = data.at(U("name")).as_string();
				if (data.has_field(U("secret")))
					this->secret = data.at(U("secret")).as_string();
				if (data.has_field(U("host")))
					this->host = data.at(U("host")).as_string();
				if (data.has_field(U("port")))
					this->port = data.at(U("port")).as_integer();
			}

			json to_json() const
			{
				json data;
				data[U("id")] = json{ this->id };
				data[U("name")] = json{ this->name };
				data[U("secret")] = json{ this->secret };
				data[U("host")] = json{ this->host };
				data[U("port")] = json{ this->port };
				return data;
			}
		};
	}
}

#endif // WEDIS_MODEL_COMPANY_HPP
