
#ifndef CORE_JSON_HPP
#define CORE_JSON_HPP

#include "exception.hpp"

#include <cpprest/json.h>

using json = ::web::json::value;

#endif // !CORE_JSON_HPP
