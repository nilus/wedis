
#ifndef CORE_EXCEPTION_HPP
#define CORE_EXCEPTION_HPP

#include <string>

namespace core
{
	namespace exception
	{
		struct bad_data_t
		{

		};

		struct not_found_t
		{
		};

		struct unauthorized_t
		{
		};

		struct method_not_found_t
		{
		};

		struct method_unsupported_t
		{
		};

		struct error_t
		{
			std::string message;

			error_t() = default;

			explicit error_t(std::string message)
				: message(std::move(message))
			{}
		};
	}
}

#endif // !CORE_EXCEPTION_HPP
