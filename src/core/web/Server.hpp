
#ifndef CORE_WEB_SERVER_HPP
#define CORE_WEB_SERVER_HPP


#include "Endpoint.hpp"
#include "utils.hpp"

#include <cpprest/http_listener.h>

namespace core
{
	namespace web
	{
		class Server final
		{
		private:

			using http_listener = ::web::http::experimental::listener::http_listener;

		private:

			const std::string name;

			http_listener listener;

			bool opened = false;

		public:

			Server() = delete;

			Server(const ::web::uri & uri, const std::string & name)
				: name(name)
				, listener(uri.to_string())
			{
				std::cout << "creating server " << name << std::endl;
			}

			~Server()
			{
				stop();
			}

		public:

			void start()
			{
				std::cout << "starting server " << name << " @ " << listener.uri().to_string() << std::endl;

				this->listener.open().wait();
				this->opened = true;
			}

			void stop()
			{
				std::cout << "stopping server " << name << " @ " << listener.uri().to_string() << std::endl;

				if (this->opened)
				{
					this->listener.close().wait();
					this->opened = false;
				}
			}

			void handle(EndpointBase & endpoint)
			{
				std::cout << "registering endpoint " << endpoint.path << " to server " << name << std::endl;

				this->listener.support(
					std::bind(&EndpointBase::handle, &endpoint, std::placeholders::_1));
			}
		};
	}
}

#endif // !CORE_WEB_SERVER_HPP
