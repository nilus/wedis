
#ifndef CORE_WEB_RESOURCE_HPP
#define CORE_WEB_RESOURCE_HPP

#include "utils.hpp"
#include "core/json.hpp"

namespace core
{
	namespace web
	{
		class Resource
		{
		public:

			using path_params_t = std::vector<std::string>;
			using status_codes = ::web::http::status_codes;

			const std::string absolute_path;
			const std::string relative_path;

		protected:

			Resource(std::string && absolute_path, std::string && relative_path)
				: absolute_path(std::move(absolute_path))
				, relative_path(std::move(relative_path))
			{}

		public:

			virtual ~Resource() = default;
		};
	}
}

#endif //CORE_WEB_RESOURCE_HPP
