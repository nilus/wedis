
#ifndef CORE_WEB_ENDPOINT_HPP
#define CORE_WEB_ENDPOINT_HPP

#include "core/json.hpp"
#include "core/web/utils.hpp"

#include <vector>

namespace core
{
	namespace web
	{
		class EndpointBase
		{
		public:

			using status_codes = ::web::http::status_codes;

			const std::string path;

		protected:

			explicit EndpointBase(std::string && path)
				: path(std::move(path))
			{}

		public:

			virtual ~EndpointBase() = default;

		protected:

			virtual void process(const http_request & request) = 0;

		public:

			void handle(const http_request & request)
			{
				try
				{
					process(request);
				}
				catch (const core::exception::bad_data_t &)
				{
					request.reply(status_codes::BadRequest);
				}
				catch (const core::exception::not_found_t &)
				{
					request.reply(status_codes::NotFound);
				}
				catch (const core::exception::unauthorized_t &)
				{
					request.reply(status_codes::Unauthorized);
				}
				catch (const ::web::json::json_exception & e)
				{
					request.reply(status_codes::BadRequest, e.what());
				}
				catch (const ::web::http::http_exception & e)
				{
					std::cout << "Could not extract json: " << e.what() << std::endl;
					request.reply(status_codes::BadRequest);
				}
				catch (const std::system_error& e)
				{
					std::wostringstream ss;
					ss << e.what() << std::endl;
					std::wcout << ss.str();
					request.reply(status_codes::InternalError);
				}
				catch (const std::exception & e)
				{
					std::cout << e.what() << std::endl;
					request.reply(status_codes::InternalError, e.what());
				}
			}
		};

		template<typename Resource>
		class Endpoint : public EndpointBase
		{
		private:

			std::vector<const Resource *> resources;

		protected:

			using http_request = ::web::http::http_request;

		protected:

			explicit Endpoint(std::string && path)
				: EndpointBase(std::move(path))
			{}

		protected:

			json extract_json(const http_request & request)
			{
				auto task = request.extract_json();
				auto jdata = task.get();

				if (jdata.is_null())
				{
					throw core::exception::bad_data_t{};
				}

				return jdata;
			}

			const Resource * match(const std::string & path)
			{
				for(auto resource : resources)
				{
					if (resource->relative_path.empty()) // accepts all
					{
						return resource;
					}

					auto length_endpoint = resource->absolute_path.length();

					if (length_endpoint > path.length())
					{
						continue;
					}

					if (path == resource->absolute_path)
					{
						return resource;
					}

					auto sub_str = path.substr(0, length_endpoint);

					if (sub_str == resource->absolute_path)
					{
						return resource;
					}
				}

				throw exception::not_found_t{};
			}

		public:

			void handle_resource(const Resource & resource)
			{
				std::cout << "registering resource " << resource.relative_path << " to endpoint " << path << std::endl;

				this->resources.push_back(&resource);
			}
		};
	}
}

#endif // CORE_WEB__ENDPOINT_HPP
