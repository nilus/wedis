
#ifndef CORE_WEB_UTILS_HPP
#define CORE_WEB_UTILS_HPP

#include <core/exception.hpp>

#include <cpprest/asyncrt_utils.h>
#include <cpprest/base_uri.h>
#include <cpprest/http_headers.h>
#include <cpprest/http_msg.h>

using http_request = ::web::http::http_request;

namespace core
{
	namespace web
	{
		using credentials_t = std::pair<std::string, std::string>;

		inline auto get_param(const std::map<std::string, std::string> params, const std::string & key)
		{
			auto itr = params.find(key);

			if (itr == params.end())
			{
				throw core::exception::bad_data_t{};
			}

			return itr->second;
		}

		inline auto credentials(const ::web::http::http_headers & headers)
		{
			auto itr = headers.find(U("Authorization"));

			if (itr != headers.end())
			{
				auto offset = std::string{ "Basic " }.size();
				auto base64 = itr->second.substr(offset);
				auto auth = ::utility::conversions::from_base64(base64);

				unsigned i;
				for (i = 0; i < auth.size(); i++)
				{
					if (auth[i] == ':')
						break;
				}

				if (i == auth.size())
				{
					throw core::exception::unauthorized_t{};
				}

				std::string username{ (char*)auth.data(), i };
				std::string password{ (char*)auth.data() + i + 1, auth.size() - i - 1 };

				return credentials_t{
					utility::conversions::to_string_t(username),
					utility::conversions::to_string_t(password) };
			}

			throw core::exception::unauthorized_t{};
		}

		inline void authenticate(
			const ::web::http::http_headers & headers,
			const credentials_t & validation)
		{
			auto provided_credentials = core::web::credentials(headers);

			if (provided_credentials != validation)
			{
				throw core::exception::unauthorized_t{};
			}
		}
	}
}

#endif // !CORE_WEB_UTILS_HPP
