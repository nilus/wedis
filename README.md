
# Building project

## Windows
Download install and setup VCPKG. Install cpprestsdk and boost in it.
In Power Shell <3 in VCPKG folder:
```
.\bootstrap-vcpkg.bat
vcpkg install boost
vcpkg install cpprestsdk
vcpkg integrate install
```

Run cmake (from build folder) with additional command:

```
cmake .. "-DCMAKE_TOOLCHAIN_FILE=C:/gaia/vcpkg/scripts/buildsystems/vcpkg.cmake"
```
